<?php
namespace augma\forms;

use std, gui, framework, augma;


class Loader extends AbstractForm
{

    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null)
    {    
        $this->fullScreen = true;
        
        $thread = new Thread(function(){
            wait(1);
            uiLater(function(){
                $this->progressIndicator->progress = 0;
            });
            $files = ["fs",
                      "fs/sys" => "dir","fs/sys/init.aef" => "fl","fs/sys/ukey.adf" => "fl","fs/sys/hkey.adf" => "fl","fs/sys/skey.adf" => "fl","fs/sys/lkey.adf" => "fl",
                      "fs/usr" => "dir","fs/usr/docs" => "dir","fs/usr/mus" => "dir","fs/usr/vid" => "dir","fs/usr/desk" => "dir"];
            $errs = 0;
            foreach ($files as $file => $type) {
                uiLater(function(){
                    if($this->progressIndicator->progress < 101) {
                        $this->progressIndicator->progress++;
                    }
                });
                if(!file_exists($file)) {
                    if($type == "dir") {
                        mkdir($file);
                    } else {
                        $errs++;
                    }
                }
                usleep(200);    
            }
            
            if($errs > 0) {
                alert($errs." файлов не прошли проверку. Дальнейшая работа Augma Desktop недоступна.");
                app()->shutdown();
            } else {
                uiLater(function(){
                    Animation::fadeOut($this->layout,500,function(){
                        $auth = app()->form("Auth");
                        $this->form("Main")->fragment->applyFragment($auth);
                    });
                });
            }
        });
        $thread->start();
    }

}
