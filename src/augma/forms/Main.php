<?php
namespace augma\forms;

use std, gui, framework, augma;


class Main extends AbstractForm
{

    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null)
    {    
        $this->fullScreen = true;
        $form = app()->form("Loader");
        $this->fragment->applyFragment($form);
    }

}
